# new line
# devops-netology
Terraform.gitignore

# игнорим путь содержащий .terraform
**/.terraform/*

# игнорим файлы в текущей дериктории содержащие tfstate с любыми символами до или после точки
*.tfstate
*.tfstate.*

# игнорим файл в текущей дериктории crash.log и файлы crash.*.log с рандомными символами между точками
crash.log
crash.*.log

# игнорим файлы в текущей дериктории с рандомными символами до точки
*.tfvars

# игнорим файлы в текущей дериктории override.tf и override.tf.json, файлы с рандомными символами до "_override.tf" и "_override.tf.json"
override.tf
override.tf.json
*_override.tf
*_override.tf.json


# игнорим файлы в текущей дериктории 
.terraformrc
terraform.rc